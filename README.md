# <b>Pre-requisite<b>

### 1) Install cmake for visual studio installer 

![cma](./cmake.PNG)

### 2) Install C++ Build tools for Visual Studio(MSVC)


# <b>Using the project</b>

### Run configuration file 
<code>
./configure.sh
</code>

![test](./msvc.gif)